Linux Vendor Firmware Service
=============================

This is the website for the Linux Vendor Firmware Service

Setting up the web service
--------------------------

You can set up the development database manually using:

    $ sudo su - postgres
    $ psql
    > CREATE USER lvfs WITH PASSWORD 'lvfs' CREATEDB;
    > CREATE DATABASE lvfs OWNER lvfs;
    > quit

Remember to edit `/var/lib/pgsql/data/pg_hba.conf` and add the `md5` auth
method for localhost.

Then create the schema using (requires first setting up virtualenv, see below.):

    FLASK_APP=lvfs/__init__.py ./env/bin/flask initdb
    FLASK_APP=lvfs/__init__.py ./env/bin/flask db stamp
    FLASK_APP=lvfs/__init__.py ./env/bin/flask db upgrade

The admin user is set as `sign-test@fwupd.org` with password `Pa$$w0rd`.

## Running locally ##

    python3 -m virtualenv env
    source env/bin/activate
    pip3 install -r requirements.txt
    make run

> Note: If not running on localhost such as inside a local VM there are 2 there
> are 2 files that should be modified:
>
> 1. Makefile to add `--host=0.0.0.0` to the end of the `make run` cmd
> 2. lvfs/flaskapp.cfg to update the `CDN_DOMAIN` to the IP of the machine.
>    This will allow images to load properly.

You may also need to install introspection dependencies.

For example on Ubuntu the following is required:

    sudo apt install -y python3-gi gcab gir1.2-libgcab-1.0

On Fedora:

    sudo dnf install \
        bsdtar \
        gcc \
        gnutls-utils \
        postgresql-devel \
        postgresql-server \
        python3-devel \
        python3-pip \
        python3-psutil \
        python3-virtualenv

You can then set up the default site settings by logging in as the admin, and
then visiting `https://127.0.0.1:5000/lvfs/settings/create` to set all the unset
config values to their defaults.

## Running a worker ##

The LVFS adds long-running tasks such as signing metadata and firmware to a
built-in task queue. The task queue can be processed using:

    PYTHONPATH=. ./env/bin/python ./lvfs/worker.py

## Generating a SSL certificate ##

IMPORTANT: The LVFS needs to be hosted over SSL.
If you want to use LetsEncrypt you can just do `certbot --nginx`.

## Installing the test key ##

Use the test GPG key (with the initial password of `fwupd`).

    gpg2 --homedir=/var/www/lvfs/.gnupg --allow-secret-key-import --import /var/www/lvfs/stable/contrib/fwupd-test-private.key
    gpg2 --homedir=/var/www/lvfs/.gnupg --list-secret-keys
    gpg2 --homedir=/var/www/lvfs/.gnupg --edit-key D64F5C21
    gpg> passwd
    gpg> trust
    gpg> quit

If passwd cannot be run due to being in a sudo session you can do:

    gpg-agent --homedir=/var/www/lvfs/.gnupg --daemon

or

    script /dev/null
    gpg2...

## Using the production key ##

Use the secure GPG key (with the long secret password).

    cd
    gpg2 --homedir=/var/www/lvfs/.gnupg --allow-secret-key-import --import fwupd-secret-signing-key.key
    gpg2 --homedir=/var/www/lvfs/.gnupg --list-secret-keys
    gpg2 --homedir=/var/www/lvfs/.gnupg --edit-key 4538BAC2
      gpg> passwd
      gpg> quit

## Creating the container

Create an image using:

    podman build .

Then run the image with:

    podman container run -e DEPLOY=application <theID>
    podman container run -e DEPLOY=metadata <theID>

## Generating metadata for pre-signed firmware ##

If the firmware is already signed with a PKCS-7 or GPG signature and is going
to be shipped out-of-band from the usual LVFS workflow then `local.py` can be
used to generate metadata for `/usr/share/fwupd/remotes.d/vendor/firmware/`.

An example of generating metadata:
```
./local.py --archive-directory /usr/share/fwupd/remotes.d/vendor/ --basename firmware --metadata /usr/share/fwupd/remotes.d/vendor/vendor.xml.gz
```

This assumes that the firmware CAB files are already in `/usr/share/fwupd/remotes.d/vendor/firmware`
and will be run on that system.
