#!/usr/bin/python3
#
# Copyright 2020 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
#
# pylint: disable=too-few-public-methods

import enum
import datetime
import hashlib

from typing import Any, Optional


class JcatBlobKind(enum.IntEnum):
    UNKNOWN = 0
    SHA256 = 1
    GPG = 2
    PKCS7 = 3
    SHA1 = 4
    BT_MANIFEST = 5
    BT_CHECKPOINT = 6
    BT_INCLUSION_PROOF = 7
    BT_VERIFIER = 8
    ED25519 = 9
    SHA512 = 10
    BT_LOGINDEX = 11


class JcatBlobFlags(enum.IntEnum):
    NONE = 0
    IS_UTF8 = 1


class JcatBlob:
    def __init__(
        self,
        kind: JcatBlobKind = JcatBlobKind.UNKNOWN,
        data: Optional[bytes] = None,
        flags: JcatBlobFlags = JcatBlobFlags.NONE,
        target: JcatBlobKind = JcatBlobKind.UNKNOWN,
    ) -> None:
        self.kind = kind
        self.data = data
        self.flags = flags
        self.target = target
        self.appstream_id = None
        self.timestamp = int(datetime.datetime.utcnow().timestamp())

    def __len__(self) -> int:
        if not self.data:
            return 0
        return len(self.data)

    def __repr__(self) -> str:
        return f"JcatBlob({self.kind!s}:{len(self):x})"

    def save(self) -> dict[str, Any]:
        node: dict[str, Any] = {}
        node["Kind"] = self.kind
        node["Flags"] = self.flags
        if self.appstream_id:
            node["AppstreamId"] = self.appstream_id
        if self.target != JcatBlobKind.UNKNOWN:
            node["Target"] = self.target
        node["Timestamp"] = self.timestamp
        if self.data:
            node["Data"] = self.data.decode()
        return node

    def load(self, node: dict[str, Any]) -> None:
        self.kind = node.get("Kind", JcatBlobKind.UNKNOWN)
        self.target = node.get("Target", JcatBlobKind.UNKNOWN)
        self.flags = node.get("Flags", 0)
        self.appstream_id = node.get("AppstreamId", None)
        self.timestamp = node.get("Timestamp", None)
        try:
            self.data = node["Data"].encode()
        except (KeyError, AttributeError):
            self.data = None

    @property
    def filename_ext(self) -> Optional[str]:
        if self.kind == JcatBlobKind.SHA256:
            return "sha256"
        if self.kind == JcatBlobKind.GPG:
            return "asc"
        if self.kind == JcatBlobKind.PKCS7:
            return "p7b"
        if self.kind == JcatBlobKind.BT_MANIFEST:
            return "btmanifest"
        if self.kind == JcatBlobKind.BT_CHECKPOINT:
            return "btcheckpoint"
        if self.kind == JcatBlobKind.BT_INCLUSION_PROOF:
            return "btinclusionproof"
        if self.kind == JcatBlobKind.BT_VERIFIER:
            return "btverifier"
        if self.kind == JcatBlobKind.ED25519:
            return "ed25519"
        if self.kind == JcatBlobKind.SHA512:
            return "sha512"
        if self.kind == JcatBlobKind.BT_LOGINDEX:
            return "btlogindex"
        return None


class JcatBlobText(JcatBlob):
    def __init__(self, kind: JcatBlobKind, data_str: str) -> None:
        JcatBlob.__init__(self, kind, data_str.encode(), JcatBlobFlags.IS_UTF8)


class JcatBlobSha1(JcatBlobText):
    def __init__(self, blob: bytes) -> None:
        data_str = hashlib.sha1(blob).hexdigest()
        JcatBlobText.__init__(self, JcatBlobKind.SHA1, data_str)


class JcatBlobSha256(JcatBlobText):
    def __init__(self, blob: bytes) -> None:
        data_str = hashlib.sha256(blob).hexdigest()
        JcatBlobText.__init__(self, JcatBlobKind.SHA256, data_str)


class JcatBlobSha512(JcatBlobText):
    def __init__(self, blob: bytes) -> None:
        data_str = hashlib.sha512(blob).hexdigest()
        JcatBlobText.__init__(self, JcatBlobKind.SHA512, data_str)
