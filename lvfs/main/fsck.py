#!/usr/bin/python3
#
# Copyright 2023 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
#
# pylint: disable=too-few-public-methods,disable=singleton-comparison

import os
import fnmatch
from typing import Optional

from flask import current_app as app

from lvfs import db
from lvfs.tasks.models import Task

# glob.glob(pathname, root_dir) is only supported in Python 3.10
def _glob_glob(pathname: str, root_dir: str) -> list[str]:
    entry_names: list[str] = []
    for entry in os.scandir(root_dir):
        if fnmatch.fnmatch(entry.name, pathname):
            entry_names.append(entry.name)
    return entry_names


def _fsck_main_cachedir(task: Task) -> None:

    # delete all existing
    task.status = "Deleting"
    db.session.commit()

    cnt: int = 0
    for fn in _glob_glob("*", root_dir=app.config["CACHE_DIR"]):
        if len(fn) != 32:
            task.add_fail(f"ignoring {fn}")
            continue
        os.remove(os.path.join(app.config["CACHE_DIR"], fn))
        cnt += 1
    task.add_pass(f"deleted {cnt} cache items")


def _fsck(task: Task, kinds: Optional[list[str]] = None) -> None:
    if not kinds or "cachedir" in kinds:
        _fsck_main_cachedir(task)
