#!/usr/bin/python3
#
# Copyright 2023 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

import os
from typing import Any

from flask import Blueprint, request, url_for, redirect, flash, render_template, g
from flask_login import login_required
from sqlalchemy.exc import NoResultFound, IntegrityError

from lvfs import db

from lvfs.util import admin_login_required
from lvfs.metadata.models import Remote

from .models import Mirror, MirrorFilter

bp_mirrors = Blueprint("mirrors", __name__, template_folder="templates")


@bp_mirrors.route("/")
@login_required
@admin_login_required
def route_list() -> Any:
    # only show mirrors with the correct group_id
    mirrors = db.session.query(Mirror).order_by(Mirror.name.asc()).all()
    return render_template("mirror-list.html", category="admin", mirrors=mirrors)


@bp_mirrors.post("/create")
@login_required
@admin_login_required
def route_create() -> Any:
    # ensure has enough data
    try:
        name = request.form["name"]
    except KeyError:
        flash("No form data found!", "warning")
        return redirect(url_for("mirrors.route_list"))
    if not name or not name.islower() or name.find(" ") != -1:
        flash("Failed to add mirror: Value needs to be a lower case word", "warning")
        return redirect(url_for("mirrors.route_list"))

    # add mirror
    try:
        mirror = Mirror(name=name, user=g.user)
        db.session.add(mirror)
        db.session.commit()
    except IntegrityError:
        db.session.rollback()
        flash("Failed to add mirror: The mirror already exists", "info")
        return redirect(url_for("mirrors.route_list"))
    flash("Added mirror", "info")
    return redirect(url_for("mirrors.route_show", mirror_id=mirror.mirror_id))


@bp_mirrors.post("/<int:mirror_id>/delete")
@login_required
@admin_login_required
def route_delete(mirror_id: int) -> Any:
    # get mirror
    try:
        mirror = db.session.query(Mirror).filter(Mirror.mirror_id == mirror_id).one()
        for mirror_item in mirror.items:
            os.remove(mirror_item.absolute_path)
        db.session.delete(mirror)
        db.session.commit()
    except NoResultFound:
        flash("No mirror found", "info")
        return redirect(url_for("mirrors.route_list"))
    except IntegrityError:
        db.session.rollback()
        flash("Cannot delete", "danger")
        return redirect(url_for("mirrors.route_list"))
    flash("Deleted mirror", "info")
    return redirect(url_for("mirrors.route_list"))


@bp_mirrors.post("/<int:mirror_id>/modify")
@login_required
@admin_login_required
def route_modify(mirror_id: int) -> Any:
    try:
        mirror = (
            db.session.query(Mirror)
            .filter(Mirror.mirror_id == mirror_id)
            .with_for_update(of=Mirror)
            .one()
        )
    except NoResultFound:
        flash("No mirror found", "info")
        return redirect(url_for("mirrors.route_list"))

    # remote
    if "remote_id" in request.form:
        try:
            remote_id: int = int(request.form["remote_id"])
        except ValueError:
            mirror.remote = None
        else:
            try:
                mirror.remote = (
                    db.session.query(Remote)
                    .filter(Remote.is_public)
                    .filter(Remote.remote_id == remote_id)
                    .one()
                )
            except NoResultFound:
                flash("No public remote found", "info")
                return redirect(url_for("mirrors.route_show", mirror_id=mirror_id))

    # properties
    mirror.enabled = bool("enabled" in request.form)
    mirror.remove_deleted = bool("remove_deleted" in request.form)
    for key in [
        "name",
        "uri",
        "auth_username",
        "auth_token",
        "comment",
    ]:
        if key in request.form:
            setattr(mirror, key, request.form[key] or None)
    db.session.commit()
    flash("Modified mirror", "info")
    return redirect(url_for("mirrors.route_show", mirror_id=mirror_id))


@bp_mirrors.route("/<int:mirror_id>/details")
@login_required
@admin_login_required
def route_show(mirror_id: int) -> Any:
    try:
        mirror = db.session.query(Mirror).filter(Mirror.mirror_id == mirror_id).one()
    except NoResultFound:
        flash("No mirror found", "info")
        return redirect(url_for("mirrors.route_list"))
    remotes = db.session.query(Remote).filter(Remote.is_public).all()
    return render_template(
        "mirror-details.html",
        category="admin",
        remotes=remotes,
        mirror=mirror,
    )


@bp_mirrors.post("/<int:mirror_id>/mirror_filter/create")
@login_required
@admin_login_required
def route_filter_create(mirror_id: int) -> Any:
    """Add a mirror filter"""
    try:
        key = request.form["key"]
        if key.lower() != key:
            flash("Invalid filter key", "warning")
            return redirect(url_for("mirrors.route_show", mirror_id=mirror_id))
        mirror = (
            db.session.query(Mirror)
            .filter(Mirror.mirror_id == mirror_id)
            .with_for_update(of=Mirror)
            .one()
        )
        mirror.filters.append(MirrorFilter(key=key, value=request.form["value"]))
        db.session.commit()
    except KeyError:
        flash("No form value", "warning")
        return redirect(url_for("mirrors.route_list"))
    except NoResultFound:
        flash("Failed to get mirror details", "warning")
        return redirect(url_for("mirrors.route_list"))
    except IntegrityError as e:
        db.session.rollback()
        flash(f"Failed to add mirror filter: {e!s}", "info")
        return redirect(url_for("mirrors.route_list"))
    flash("Added filter", "info")
    return redirect(url_for("mirrors.route_show", mirror_id=mirror_id))


@bp_mirrors.post("/<int:mirror_id>/mirror_filter/<int:mirror_tag_id>/delete")
@login_required
@admin_login_required
def route_filter_delete(mirror_id: int, mirror_tag_id: int) -> Any:
    """Delete a mirror filter"""
    try:
        mirror_filter = (
            db.session.query(MirrorFilter)
            .filter(MirrorFilter.mirror_tag_id == mirror_tag_id)
            .join(Mirror)
            .filter(Mirror.mirror_id == mirror_id)
            .with_for_update(of=Mirror)
            .one()
        )
        db.session.delete(mirror_filter)
        db.session.commit()
    except NoResultFound:
        flash("Failed to get mirror", "warning")
        return redirect(url_for("mirrors.route_list"))
    except IntegrityError:
        db.session.rollback()
        flash("Failed to remove mirror filter", "info")
        return redirect(url_for("mirrors.route_list"))
    flash("Deleted filter", "info")
    return redirect(url_for("mirrors.route_show", mirror_id=mirror_filter.mirror_id))
