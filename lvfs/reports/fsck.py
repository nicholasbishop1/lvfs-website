#!/usr/bin/python3
#
# Copyright 2022 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
#
# pylint: disable=too-few-public-methods,unused-argument,protected-access

from typing import Optional

from pkgversion import vercmp

from lvfs import db
from lvfs.tasks.models import Task
from .models import Report, ReportAttribute, REPORT_ATTR_MAP
from .utils import _report_key_valid


def _fsck_reports_fixup_old_attrs(self: Report, task: Task) -> None:
    # fix up report attrs
    fixed: list[str] = []
    for key, value in REPORT_ATTR_MAP.items():
        attr = self.get_attribute_by_key(key)
        if attr:
            attr.key = value
            if key not in fixed:
                fixed.append(key)

    # check is-upgrade is set
    attr_new = self.get_attribute_by_key("VersionNew")
    attr_old = self.get_attribute_by_key("VersionOld")
    attr_flags = self.get_attribute_by_key("Flags")
    if (
        not attr_flags
        and attr_old
        and attr_new
        and vercmp(attr_new.value, attr_old.value) > 0
    ):
        self.attributes.append(ReportAttribute(key="Flags", value="is-upgrade"))
        fixed.append("Flags")

    if fixed:
        task.add_fail(
            "Database::Reports",
            f"Fixed up {','.join(fixed)} keys",
        )


def _fsck_reports_fixup_invalid_attrs(self: Report, task: Task) -> None:
    # delete crazy values
    for attr in self.attributes:
        if not _report_key_valid(attr.key):
            task.add_fail(
                "Database::Reports",
                f"Deleted {attr.key}={attr.value}",
            )
            db.session.delete(attr)


def _fsck(self: Report, task: Task, kinds: Optional[list[str]] = None) -> None:
    if not kinds or "attrs" in kinds:
        _fsck_reports_fixup_old_attrs(self, task)
        _fsck_reports_fixup_invalid_attrs(self, task)
