# Custom template
"""empty message

Revision ID: 4eb6cb484b9c
Revises: eccab9066ce3
Create Date: 2023-05-16 10:27:23.369371

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "4eb6cb484b9c"
down_revision = "eccab9066ce3"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("client_acls", schema=None) as batch_op:
        batch_op.add_column(sa.Column("user_agent", sa.Text(), nullable=True))
        batch_op.add_column(sa.Column("resource", sa.Text(), nullable=True))


def downgrade():
    with op.batch_alter_table("client_acls", schema=None) as batch_op:
        batch_op.drop_column("resource")
        batch_op.drop_column("user_agent")
