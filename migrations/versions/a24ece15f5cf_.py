# Custom template
"""empty message

Revision ID: a24ece15f5cf
Revises: 5e80fba7a4f0
Create Date: 2023-03-30 15:17:57.407040

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "a24ece15f5cf"
down_revision = "5e80fba7a4f0"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("client_acls", schema=None) as batch_op:
        batch_op.add_column(sa.Column("cnt", sa.Integer(), nullable=True))
        batch_op.add_column(sa.Column("mtime", sa.DateTime(), nullable=True))


def downgrade():
    with op.batch_alter_table("client_acls", schema=None) as batch_op:
        batch_op.drop_column("mtime")
        batch_op.drop_column("cnt")
