# Custom template
"""empty message

Revision ID: 8c5b86d6c9d1
Revises: 9625c0091f41
Create Date: 2023-05-04 16:50:26.530102

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "8c5b86d6c9d1"
down_revision = "9625c0091f41"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("firmware", schema=None) as batch_op:
        batch_op.alter_column(
            "checksum_signed_sha1", existing_type=sa.VARCHAR(length=40), nullable=True
        )
        batch_op.alter_column(
            "checksum_signed_sha256", existing_type=sa.VARCHAR(length=64), nullable=True
        )
    with op.batch_alter_table("firmware_revisions", schema=None) as batch_op:
        batch_op.add_column(
            sa.Column("checksum_sha1", sa.String(length=40), nullable=True)
        )
        batch_op.add_column(
            sa.Column("checksum_sha256", sa.String(length=64), nullable=True)
        )
        batch_op.create_index(
            batch_op.f("ix_firmware_revisions_checksum_sha1"),
            ["checksum_sha1"],
            unique=False,
        )
        batch_op.create_index(
            batch_op.f("ix_firmware_revisions_checksum_sha256"),
            ["checksum_sha256"],
            unique=False,
        )


def downgrade():
    with op.batch_alter_table("firmware_revisions", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("ix_firmware_revisions_checksum_sha256"))
        batch_op.drop_index(batch_op.f("ix_firmware_revisions_checksum_sha1"))
        batch_op.drop_column("checksum_sha256")
        batch_op.drop_column("checksum_sha1")
    with op.batch_alter_table("firmware", schema=None) as batch_op:
        batch_op.alter_column(
            "checksum_signed_sha256",
            existing_type=sa.VARCHAR(length=64),
            nullable=False,
        )
        batch_op.alter_column(
            "checksum_signed_sha1", existing_type=sa.VARCHAR(length=40), nullable=False
        )
